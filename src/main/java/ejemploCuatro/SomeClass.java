package ejemploCuatro;

public class SomeClass {

    /**
     * retorna el doble de un numero
     */
    public int doubleANumber(int num) {
        return num * 2;
    }

    /**
     * compara si una cadena es igual a otra
     */
    public boolean returnABoolean(String inputData) {
        if ("Save".equalsIgnoreCase(inputData)) {
            return true;
        } else {
            return false;
        }
    }

    /**
    * compara si una cadena es igual a otra
    */
    public void voidFoo(String inputData) {
        if ("Ok".equalsIgnoreCase(inputData)) {
            System.out.println("doing something.");;
        } else {
            throw new IllegalArgumentException("Bad argument:" + inputData);
        }
    }
}