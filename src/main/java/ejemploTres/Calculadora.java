package ejemploTres;

import java.util.StringTokenizer;

public class Calculadora {

    /**
     * retorna la suma de dos numeros
     */
    public int add(int a, int b) {
        return a + b;
    }

    /**
     * retorna la multiplicacion de dos numeros
     */
    public int multiply(int a, int b) {
        return a * b;
    }

    /**
     * retorna el mayor valor
     */
    public static int findMax(int arr[]) {

        //int max = 0;
        //for (int i = 0; i < arr.length; i++) {
        //    if (max < arr[i])
        //        max = arr[i];
        //}
        //return max;


        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (max < arr[i])
                max = arr[i];
        }
        return max;
    }

    /**
     * retorna el cubo de un numero entero
     */
    public static int cube(int n) {
        return n * n * n;
    }

    /**
     * retorna una palabra invertida
     */
    public static String reverseWord(String str) {

        StringBuilder result = new StringBuilder();
        StringTokenizer tokenizer = new StringTokenizer(str, " ");

        while (tokenizer.hasMoreTokens()) {
            StringBuilder sb = new StringBuilder();
            sb.append(tokenizer.nextToken());
            sb.reverse();

            result.append(sb);
            result.append(" ");
        }
        return result.toString();
    }

}
