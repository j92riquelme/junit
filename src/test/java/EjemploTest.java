import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;


public class EjemploTest {

    /**
     * si el array esta vacio la prueba unitaria es correcta
     */
    @Test
    public void method() {
        Assert.assertTrue(new ArrayList().isEmpty());
    }

    /**
     * si ocurre un error de IndexOutOfBoundsException la prueba unitaria es correcta
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void outOfBounds() {
        new ArrayList<Object>().get(1);
    }

    /**
     * si se ejecuta el metodo en menos de 100 milisegundos de la prueba unitaria es correcta
     */
    @Test(timeout = 100)
    public void infinity() {
        try {
            Thread.sleep(80);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
