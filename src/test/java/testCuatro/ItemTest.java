package testCuatro;// https://springframework.guru/java-hashmap-vs-hashtable/
import ejemploCuatro.Item;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static junit.framework.TestCase.*;

public class ItemTest {

    private Item item, item1, item2, item3;

    @Before
    public void setUp() {
        item = new Item("Water bottle", 20);
        item1 = new Item("Plate", 30);
        item2 = new Item("Spoon", 10);
        item3 = new Item("Glass", 15);
    }

    @After
    public void tearDown() {
        item = item1 = item2 = item3 = null;
    }

    @Test
    public void ItemWithHashMapTest() {
        HashMap<Item, Integer> hashMap = new HashMap<Item, Integer>();
        hashMap.put(item, 10);
        hashMap.put(item1, 5);
        hashMap.put(item2, 10);
        hashMap.put(item3, 3);
        System.out.println("Size of HashMap " + hashMap.size());
        assertEquals(4, hashMap.size());
        for (Map.Entry entry : hashMap.entrySet()) {
            System.out.println(entry.getKey().toString() + "-" + entry.getValue());
        }
        Item newItemAsKeyInHashMap = new Item("Water bottle", 20);
        assertTrue(hashMap.containsKey(newItemAsKeyInHashMap));
        assertTrue(hashMap.containsValue(hashMap.get(newItemAsKeyInHashMap)));
    }

    @Test
    public void ItemWithHashtableTest() {
        Hashtable<Item, Integer> hashtable = new Hashtable<Item, Integer>();
        hashtable.put(item, 10);
        hashtable.put(item1, 5);
        hashtable.put(item2, 10);
        hashtable.put(item3, 3);
        hashtable.put(item1,16);
        assertEquals(4, hashtable.size());
        System.out.println("Size of Hashtable "+ hashtable.size());
        for(Map.Entry entry: hashtable.entrySet()) {
            System.out.println(entry.getKey().toString() + "-" + entry.getValue());
        }
        Item itemAsKeyInHashtable = new Item("Water bottle", 20);
        assertTrue(hashtable.containsKey(itemAsKeyInHashtable));
        assertTrue(hashtable.containsValue(hashtable.get(itemAsKeyInHashtable)));
    }

    @Test(expected = ConcurrentModificationException.class)
    public void ConcurrentModificationExceptionTest() {
        HashMap<Item, Integer> hashMap = new HashMap<Item, Integer>();
        hashMap.put(item, 10);
        hashMap.put(item1, 5);
        hashMap.put(item2, 10);
        for(Map.Entry<Item, Integer> entry: hashMap.entrySet()) {
            Integer value = entry.getValue();
            if(value == 5) {
                hashMap.remove(entry.getKey());
            }
        }
    }

    @Test
    public void ConcurrentModificationHashMapIterationTest() {
        HashMap<Item, Integer> hashMap = new HashMap<Item, Integer>();
        hashMap.put(item, 10);
        hashMap.put(item1, 5);
        hashMap.put(item2, 10);
        assertEquals(3,hashMap.size());
        for(Iterator<Map.Entry<Item, Integer>> it = hashMap.entrySet().iterator(); it.hasNext();) {
            Map.Entry<Item, Integer> entry = it.next();
            Integer value = entry.getValue();
            if(value == 5) {
                it.remove();
                System.out.println("ejemploCuatro.Item with value 5 safely removed from HashMap");
            }
        }
        assertEquals(2,hashMap.size());

    }

    @Test(expected=ConcurrentModificationException.class)
    public void ExceptionWithHashtableTest() {
        Hashtable<Item, Integer> hashtable = new Hashtable<Item, Integer>();
        hashtable.put(item, 10);
        hashtable.put(item1, 5);
        hashtable.put(item2, 10);
        for (Map.Entry<Item, Integer> entry : hashtable.entrySet()) {
            Integer value = entry.getValue();
            if (value == 5) {
                hashtable.remove(entry.getKey());
            }
        }
    }

    public void ConcurrentModificationWithHashtableEnumerationTest() {
        Hashtable<Item, Integer> hashtable = new Hashtable<Item, Integer>();
        hashtable.put(item, 5);
        hashtable.put(item1, 10);
        hashtable.put(item2, 15);
        assertEquals(3,hashtable.size());
        Enumeration<Item> en = hashtable.keys();
        while(en.hasMoreElements()) {
            Item key = en.nextElement();
            Integer value = hashtable.get(key);
            System.out.println(key + "-" + value);
            if(key.getItem().equals("Plate")) {
                hashtable.remove(key);
                System.out.println("ejemploCuatro.Item "+key+" safely removed from Hashtable");
            }
        }
        assertEquals(2,hashtable.size());

    }

    @Test
    public void NullCheckInHashMapTest() {
        HashMap<Item, Integer> hashMap = new HashMap<Item, Integer>();
        hashMap.put(item, 10);
        hashMap.put(item1, null);
        hashMap.put(null, 15);
        hashMap.put(null, null);
        for(Map.Entry entry: hashMap.entrySet()) {
            System.out.println(entry.getKey() + "-" + entry.getValue());
        }
        assertEquals(3, hashMap.size());
        assertNull(null,hashMap.get(null));
        System.out.println("Null key :"+ hashMap.get(null));
    }

    @Test(expected = NullPointerException.class)
    public void NullCheckInHashtableTest() {
        Hashtable<Item, Integer> hashtable = new Hashtable<Item, Integer>();
        hashtable.put(item, 10);
        hashtable.put(item1, 5);
        hashtable.put(null, null);
        System.out.println("Hashtable Null key "+ hashtable.get(null));
    }

}