package testCuatro;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.Duration;
import java.util.Random;

import ejemploCuatro.SomeClass;
import org.junit.jupiter.api.*;

// JUnit 5
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SomeClass5Test {

    private SomeClass classUnderTest;
    private TestInfo testInfo;
    private TestReporter testReporter;

    @BeforeEach
    @Disabled
    public void setup(TestInfo testInfo, TestReporter terstReporter ) {
        this.testInfo = testInfo;
        this.testReporter = terstReporter;
        classUnderTest = new SomeClass();
    }

    @RepeatedTest(120)
    @Order(1)
    public void test_doubleANumber() {
        Random r = new Random();
        int valorDado = r.nextInt(100)+1;
        System.out.println(valorDado + " -> " + valorDado * 2);
        assertEquals(valorDado * 2, classUnderTest.doubleANumber(valorDado), "it should return 6");
    }

    @Disabled
    public void test_not_executed() {
        fail("It should not executed");
    }

    @Test
    @DisplayName("It should return false when input data isn't Save")
    @Order(2)
    public void test_returnBooleanFoo_false() {
        boolean shouldReturnFalse = classUnderTest.returnABoolean("NA");
        assertFalse(shouldReturnFalse);
    }

    @Test
    @DisplayName("It should return true when input data is Save")
    @Order(3)
    public void test_returnBooleanFoo_true() {
        boolean shouldReturnTrue = classUnderTest.returnABoolean("Save");
        assertTrue(shouldReturnTrue);
        testReporter.publishEntry(testInfo.getDisplayName());
    }

    @Test
    @Order(4)
    public void test_voidFoo() throws IllegalAccessException {

        try {
            classUnderTest.voidFoo("OK");
        } catch (Exception e) {
            fail("Should not throw exception");
        }
    }

    @Test
    @Order(5)
    public void test_voidFoo_exception() throws IllegalAccessException {
        assertThrows(IllegalArgumentException.class, () -> {
            classUnderTest.voidFoo("NA");
        });

    }

    @Test
    @Order(6)
    public void test_timeout() {
        assertTimeout(Duration.ofMillis(1), ()-> classUnderTest.doubleANumber(1000));
    }


}