package testTres;

import ejemploTres.Calculadora;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

import static junit.framework.TestCase.assertEquals;

public class CalculadoraPart1Test extends TestCase {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("before class");
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("before");
    }

    @Test
    public void testFindMax(){
        assertEquals(4, Calculadora.findMax(new int[]{1,3,4,2}));
        assertEquals(-1,Calculadora.findMax(new int[]{-12,-1,-3,-4,-2}));
        assertEquals(4,Calculadora.findMax(new int[]{-12,-1,-3,4,0}));
    }

    @Test
    public void testCube(){
        assertEquals(64,Calculadora.cube(4));
    }

    @Test
    public void testReverseWord(){
        assertEquals("im erbmon se dcba ",Calculadora.reverseWord("mi nombre es abcd"));
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("after");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("after class");
    }


}
